<?php
switch ($service[1]) {
    // Validations
    case "summary":
        require __DIR__."/summary.php";
        break;
    case 'cash':
        require __DIR__."/cash.php";
        break;
    case 'virtual':
        require __DIR__."/virtual.php";
        break;
    case 'add-cash':
        require __DIR__."/add-cash.php";
        break;
    case 'spend-virt':
        require __DIR__."/spend-virt.php";
        break;
    case 'spend-cash':
        require __DIR__."/spend-cash.php";
        break;
    case 'earn-cash':
        require __DIR__."/earn-cash.php";
        break;
    case 'earn-virt':
        require __DIR__."/earn-virt.php";
        break;
    default:
        # code...
        break;
}
