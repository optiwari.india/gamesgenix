<?php
switch ($service[1]) {
    // Validations
    case "list":
        require __DIR__."/list.php";
        break;
    case 'create':
        require __DIR__."/create.php";
        break;
    case 'accept':
        require __DIR__."/accept.php";
        break;
    case 'update':
        require __DIR__."/update.php";
        break;
    case 'status':
        require __DIR__."/status.php";
        break;
    default:
        # code...
        break;
}
