-- Adminer 4.7.1 MySQL dump
create database gamesGenix;
use gamesGenix;
SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `activity_logs`;
CREATE TABLE `activity_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `activity` text NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  CONSTRAINT `activity_logs_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `challenge_game`;
CREATE TABLE `challenge_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game` enum('chess','ludo') NOT NULL,
  `room_name` varchar(50) NOT NULL,
  `participants` text NOT NULL,
  `challenge_amount` float(10,2) NOT NULL,
  `gamestatus` text NOT NULL,
  `gameresults` text NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `otp`;
CREATE TABLE `otp` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `phone` varchar(10) NOT NULL,
  `otp` varchar(6) NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `otp` (`id`, `phone`, `otp`, `added_on`) VALUES
(2,	'8130202878',	'047002',	'2019-10-30 05:12:29'),
(4,	'8193020287',	'204647',	'2019-10-30 05:54:22'),
(6,	'9450999364',	'927791',	'2019-10-30 06:12:35'),
(8,	'97',	'351001',	'2019-10-31 11:01:27'),
(11,	'9015032155',	'351722',	'2019-11-04 10:41:13'),
(13,	'8447083777',	'046800',	'2019-11-09 09:11:31');

DROP TABLE IF EXISTS `practice_game`;
CREATE TABLE `practice_game` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `game` enum('chess','ludo') NOT NULL,
  `room_name` varchar(50) NOT NULL,
  `participants` text NOT NULL,
  `gamestats` text NOT NULL,
  `gameresults` text NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tournament`;
CREATE TABLE `tournament` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entryfee` float(10,2) NOT NULL,
  `game` enum('Ludo','Chess') NOT NULL,
  `participants` int(11) NOT NULL,
  `prizeslab` text NOT NULL,
  `conducted_on` datetime NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tournament_participation`;
CREATE TABLE `tournament_participation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `tournament` int(11) NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `user` (`user`),
  KEY `tournament` (`tournament`),
  CONSTRAINT `tournament_participation_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`),
  CONSTRAINT `tournament_participation_ibfk_2` FOREIGN KEY (`tournament`) REFERENCES `tournament` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `tournament_status`;
CREATE TABLE `tournament_status` (
  `id` int(11) NOT NULL,
  `tournament` int(11) NOT NULL,
  `participants` text NOT NULL,
  `room_allocated` varchar(50) NOT NULL,
  `game_started_on` datetime NOT NULL,
  `game_status` text NOT NULL,
  `time_taken` time NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  KEY `tournament` (`tournament`),
  CONSTRAINT `tournament_status_ibfk_1` FOREIGN KEY (`tournament`) REFERENCES `tournament` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `transaction`;
CREATE TABLE `transaction` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `wallet` int(11) NOT NULL,
  `description` text NOT NULL,
  `amount` float(10,2) NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `wallet` (`wallet`),
  CONSTRAINT `transaction_ibfk_1` FOREIGN KEY (`wallet`) REFERENCES `wallet` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(50) NOT NULL,
  `name` text NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` text NOT NULL,
  `pass` varchar(32) NOT NULL,
  `added_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`),
  UNIQUE KEY `phone` (`phone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `user` (`id`, `user`, `name`, `phone`, `email`, `pass`, `added_on`) VALUES
(2,	'optiwari',	'Om Prakash Tiwari',	'8130202879',	'optiwari@digitlism.in',	'3e89f4eac84c8f52e0c0bc85a35b2241',	'2019-10-30 06:03:01'),
(3,	'optiwari1',	'Om Prakash Tiwari',	'9311150364',	'optiwari.india@gmail.com',	'3e89f4eac84c8f52e0c0bc85a35b2241',	'2019-10-30 06:11:33'),
(5,	'shakir',	'shakir',	'8920847239',	'shakir@digitlism.in',	'3e89f4eac84c8f52e0c0bc85a35b2241',	'2019-10-30 06:20:31'),
(6,	'deepu',	'deepanshu',	'9717773132',	'gamesgenix@gmail.com',	'827ccb0eea8a706c4c34a16891f84e7b',	'2019-10-31 11:04:10'),
(7,	'Niraj_k',	'Niraj Kumar',	'9873086267',	'nirajkumar0809@gmail.com',	'cf706a8373fb9428318723af331c596f',	'2019-11-04 10:11:29'),
(8,	'priyanka',	'priyanka',	'8700261165',	'kolhipriyana@gmail.com',	'827ccb0eea8a706c4c34a16891f84e7b',	'2019-11-04 12:04:05'),
(9,	'krishna',	'krishna',	'9716390979',	'nayaknou@gmail.com',	'900150983cd24fb0d6963f7d28e17f72',	'2019-11-09 10:55:19');

DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `info` text NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user`,`name`),
  CONSTRAINT `userinfo_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `wallet`;
CREATE TABLE `wallet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` int(11) NOT NULL,
  `type` enum('cash','virtual') NOT NULL,
  `amount` float(10,2) NOT NULL,
  `updated_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_type` (`user`,`type`),
  CONSTRAINT `wallet_ibfk_1` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- 2019-11-12 09:53:03