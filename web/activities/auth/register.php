<?php
$name = $r['name'] ?? '';
$phone = $r['phone'] ?? '';
$email = $r['email'] ?? '';
$user = $r['user'] ?? '';
$pass = $r['pass'] ?? '';
$cnfpass = $r['cnfpass'] ?? '';
$otp = $r['otp'] ?? '';

if ($pass == $cnfpass) {
    $temp = $db->select("otp", "*", "where phone='{$phone}' and otp='{$otp}'");
    if (count($temp) == 0) {
        $resp['status'] = 'error';
        $resp['error'] = "Invalid OTP";
    } else {
        $db->insert("user",
            [
                "user" => $user,
                "name" => $name,
                "phone" => $phone,
                "email" => $email,
                "pass" => $pass,
            ]
        );
        $db->query("delete from otp where phone='{$phone}'");
        $usr = $db->select("user", "*", "where phone='{$phone}'");
        $usr = $usr[0];
        unset($usr['pass']);
        $resp['status'] = "success";
        $resp['user'] = $usr;
    }
} else {
    $resp['status'] = 'error';
    $resp['error'] = "Passwords not matching";
}
