<?php
$email = explode("@", $r['email']);
$resp['status'] = "error";
if (count($email) != 2) {
    $resp['error'] = "Please type your email carefully";
} elseif (!filter_var($email[0] . "@" . $email[1], FILTER_VALIDATE_EMAIL)) {
    $resp['error'] = "Please type your email carefully";
} else{
    $rec=dns_get_record($email[1],DNS_MX);
    if (count($rec)!=0) {
        $resp['status'] = 'ok';
        $resp['error']="";
    } else {
        $resp['error'] = "The email you entered is not valid. Please enter a valid email.";
    }
}
