<?php
switch ($service[1]) {
    // Validations
    case "list":
        require __DIR__."/list.php";
        break;
    case 'create':
        require __DIR__."/create.php";
        break;
    case 'open':
        require __DIR__."/open.php";
        break;
    case 'participate':
        require __DIR__."/participate.php";
        break;
    case 'play':
        require __DIR__."/play.php";
        break;
    case 'status-update':
        require __DIR__."/status-update.php";
        break;
    case 'status':
        require __DIR__."/status.php";
        break;
    case 'winners':
        require __DIR__."/winners.php";
        break;
    default:
        # code...
        break;
}
