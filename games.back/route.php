<?php

header("Access-Control-Allow-Origin: *");
$service = explode("/", $_REQUEST['path'] ?? '');
$r = json_decode(file_get_contents('php://input'), 1);
switch ($service[0]) {
    // Validations
    case 'isname':
        require "activities/validation/name.php";
        break;
    case 'isusername':
        require "activities/validation/user.php";
        break;
    case 'isphone':
        require "activities/validation/phone.php";
        break;
    case 'isemail':
        require "activities/validation/email.php";
        break;
    case 'getotp':
        require "activities/auth/get_otp.php";
        break;
    case 'register':
        require "activities/auth/register.php";
        break;
    case 'login-otp':
        require 'activities/auth/login_otp.php';
        break;
    case 'login-pass':
        require 'activities/auth/login_pass.php';
        break;
    default:
        # code...
        break;
}
