<?php
if (!preg_match("/^(?=[a-zA-Z ]{2,50})/", $r['name'])) {
    $resp['status'] = "error";
    $resp['error'] = "Your name is not matching the valid pattern.";
    $resp['name'] = $r['name'];
}